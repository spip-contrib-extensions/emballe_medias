<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/emballe_medias.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'emballe_medias_description' => 'Formulaire configurable qui emballe les medias depuis l’espace public...',
	'emballe_medias_nom' => 'Emballe Medias',
	'emballe_medias_slogan' => 'Emballer ses documents proprement'
);
